# Python Scripts PPG

## Librerias requeridas

```
matplotlib
scipy
numpy
```

Instalar con el siguiente comando:

```
pip install matplotlib scipy numpy
```

# Funcionamiento

Crear un archivo llamado  signal.dat el cual es un archivo de texto con valores separados por coma y final de linea, como el siguiente: 
```
501,
499,
499,
499,
500,
501,
505,
518,
570,
...
...
...
...
558
```

Luego ejecutar el script python deseado:
```
python cubic_interp.py
```

El script automaticamente tomara los valores y los mostrará en un gráfico con matplotlib.
