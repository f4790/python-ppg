import numpy as np
import matplotlib.pyplot as plt
import os

txt_files = [f for f in os.listdir('.') if f.endswith('.txt') or f.endswith('.dat')]
if len(txt_files) != 1:
    raise ValueError('should be only one txt file in the current directory and must be a valid text formats: (txt, dat, csv)')

ppg_data = open(txt_files[0], 'r')

yaxis = []

for line in ppg_data.readlines():
    yaxis.append(int(line.strip(',\n')))


# Dataset
x = np.array(list(range(0, len(yaxis))))
y = np.array(yaxis)

# Ploteando el grafico
plt.plot(x, y)
plt.title("Curve plotted using the given points")
plt.xlabel("X: Time")
plt.ylabel("Y: Signal")
plt.show()
