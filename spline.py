import numpy as np
import numpy as np
from scipy.interpolate import make_interp_spline
import matplotlib.pyplot as plt
import os

txt_files = [f for f in os.listdir('.') if f.endswith('.txt') or f.endswith('.dat')]
if len(txt_files) != 1:
    raise ValueError('should be only one txt file in the current directory and must be a valid text formats: (txt, dat, csv)')

ppg_data = open(txt_files[0], 'r')

yaxis = []

for line in ppg_data.readlines():
    yaxis.append(int(line.strip(',\n')))


# Dataset
x = np.array(list(range(0, len(yaxis))))
y = np.array(yaxis)

X_Y_Spline = make_interp_spline(x, y)

# Retornar cada uno de los numeros espaciados
# sobre un intervalo especificado
X_ = np.linspace(x.min(), x.max(), 500)
Y_ = X_Y_Spline(X_)

# Ploteando el grafico
plt.plot(X_, Y_)
plt.title("Plot Smooth Curve Using the scipy.interpolate.make_interp_spline() Class")
plt.xlabel("X: Time")
plt.ylabel("Y: Signal")
plt.show()
